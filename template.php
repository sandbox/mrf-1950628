<?php

/**
 * @file
 * Functions to support theming in the Seven theme.
 */

/**
 * Implements hook_preprocess_HOOK() for maintenance-page.tpl.php.
 */
function seven_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  // the markup for the maintenance page is all in the single
  // maintenance-page.tpl.php template. So, to have what's done in
  // seven_preprocess_html() also happen on the maintenance page, it has to be
  // called here.
  seven_preprocess_html($vars);
}

/**
 * Implements hook_preprocess_HOOK() for html.tpl.php.
 */
function seven_preprocess_html(&$vars) {
  drupal_add_library('system', 'normalize');
  // Add conditional CSS for IE8 and below.
  drupal_add_css(path_to_theme() . '/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
}

/**
 * Implements hook_preprocess_HOOK() for page.html.twig.
 */
function seven_preprocess_page(&$vars) {
  $vars['primary_local_tasks'] = $vars['tabs'];
  unset($vars['primary_local_tasks']['#secondary']);
  $vars['secondary_local_tasks'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => isset($vars['tabs']['#secondary']) ? $vars['tabs']['#secondary'] : '',
  );
}

/**
 * Implements hook_preprocess_HOOK() for node-add-list.html.twig.
 */
function seven_preprocess_node_add_list(&$variables) {
  if (!empty($variables['content'])) {
    $options = array('html' => TRUE);
    foreach ($variables['content'] as $type) {
      $variables['types'][$type->type]['label'] = check_plain($type->name);
      $variables['types'][$type->type]['path'] = url('node/add/' . $type->type);
    }
  }
}

 /**
  * Overrides theme_custom_block_add_list().
  * Overrides variables for custom-block-add-list.html.twig
  * Add variables for the label and the path separately.
  *
  * Displays the list of available custom block types for creation.
  *
  */
function seven_preprocess_custom_block_add_list(&$variables) {
  foreach ($variables['content'] as $type) {
    $variables['types'][$type->id]['label'] = check_plain($type->label());
    $variables['types'][$type->id]['path'] = url('block/add/' . $type->id);
  }
}

/**
 * Overrides theme_admin_block_content().
 *
 * Uses an unordered list markup in both compact and extended mode.
 */
function seven_admin_block_content($variables) {
  $content = $variables['content'];
  $output = '';
  if (!empty($content)) {
    $output = system_admin_compact_mode() ? '<ul class="admin-list compact">' : '<ul class="admin-list">';
    foreach ($content as $item) {
      $output .= '<li>';
      $content = '<span class="label">' . filter_xss_admin($item['title']) . '</span>';
      $options = $item['localized_options'];
      $options['html'] = TRUE;
      if (isset($item['description']) && !system_admin_compact_mode()) {
        $content .= '<div class="description">' . filter_xss_admin($item['description']) . '</div>';
      }
      $output .= l($content, $item['href'], $options);
      $output .= '</li>';
    }
    $output .= '</ul>';
  }
  return $output;
}

/**
 * Implements hook_preprocess_HOOK() for tablesort-indicator.html.twig.
 */
function seven_preprocess_tablesort_indicator(&$variables) {
  $style = $variables['style'];
  $theme_path = drupal_get_path('theme', 'seven');
  if ($variables['style'] == 'asc') {
    $variables['image'] = theme('image', array('uri' => $theme_path . '/images/arrow-asc.png', 'alt' => t('sort ascending'), 'width' => 13, 'height' => 13, 'title' => t('sort ascending')));
  }
  else {
    $variables['image'] = theme('image', array('uri' => $theme_path . '/images/arrow-desc.png', 'alt' => t('sort descending'), 'width' => 13, 'height' => 13, 'title' => t('sort descending')));
  }
}

/**
 * Implements hook_preprocess_install_page().
 */
function seven_preprocess_install_page(&$variables) {
  drupal_add_js(drupal_get_path('theme', 'seven') . '/js/mobile.install.js');
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Changes vertical tabs to container and adds meta information.
 */
function seven_form_node_form_alter(&$form, &$form_state) {
  $node = $form_state['controller']->getEntity($form_state);

  $form['#theme'] = array('node_edit_form');
  $form['#attached'] = array(
    'css' => array(drupal_get_path('module', 'node') . '/node.edit.admin.css'),
  );

  $form['advanced']['#type'] = 'container';
  $form['meta'] = array (
    '#type' => 'fieldset',
    '#attributes' => array('class' => array('entity-meta-header')),
    '#type' => 'container',
    '#group' => 'advanced',
    '#weight' => -100,
    'published' => array(
      '#type' => 'item',
      '#wrapper_attributes' => array('class' => array('published')),
      '#markup' => !empty($node->status) ? t('Published') : t('Not published'),
      '#access' => !empty($node->nid),
    ),
    'changed' => array(
      '#type' => 'item',
      '#wrapper_attributes' => array('class' => array('changed', 'container-inline')),
      '#title' => t('Last saved'),
      '#markup' => !$node->isNew() ? format_date($node->changed, 'short') : t('Not saved yet'),
    ),
    'author' => array(
      '#type' => 'item',
      '#wrapper_attributes' => array('class' => array('author', 'container-inline')),
      '#title' => t('Author'),
      '#markup' => user_format_name(user_load($node->uid)),
    ),
  );
  $form['revision_information']['#type'] = 'container';
  $form['revision_information']['#group'] = 'meta';
}
